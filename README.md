# Projeto 4: BitTorrent

#### Acadêmico: Paulo Henrique de Abreu Neiva 
#### Disciplina: Aplicações Distribuidas

---
# Especificação do Trabalho
[Projeto 4: P2P (Moodle)](https://ead.inf.ufg.br/mod/forum/view.php?id=61397)

## Sobre o BitTorrent

O Desenvolvedor norte-americano, Bram Cohen, idealizou uma forma de transferir arquivos, que permitia baixar partes dos arquivos de varias fontes (pessoas/computadores) ao mesmo tempo, o que aumentava a velocidade total de transferência[]. A partir desta ideia surge o BitTorrent.

De acordo com a especificação do BitTorrent, este é um protocolo para distribuição de arquivos. O BitTorrent é uma rede P2P, onde cada um dos usuários assume papel de servidor (fornece arquivos) e de cliente (recebe arquivos), assim são chamados de _peer_. Os _peers_ (pares) efetuam download de blocos de tamanhos fixos, normalmente de 256 Kbytes.

 P2P  (do inglês _peer-to-peer_) é uma arquitetura de redes de computadores em que cada um dos pontos (computadores) ou nós da rede realiza funções, tanto de cliente quanto como servidor, permitindo assim o compartilhamento de serviços e dados sem a necessidade de um servidor central.

O conceito por trás dos peers se baseiam em download e upload, onde quando um peer está fazendo download, o mesmo é chamado de leecher (sugador) e quando o mesmo faz upload, ele é chamado de seeder (semeador).


A imagem a seguir, podemos melhor observar como funciona o BitTorrent.

![Distribuição_BitTorrent](imagens/estrutura-de-distribuicao-bittorrent.PNG)

_Disponível em [5]_

Para transferências de arquivos por BitTorrent, é necessário um arquivo com extensão do tipo .torrent a qual contém os metadados, que são informações sobre os arquivos que formam o torrent e as informações sobre o Tracker - que é o servidor, o qual tem função apenas de conectar os usuários ou mapear os clientes conectados de acordo com o conteúdo desejado. Então, é necessário registrar em um tracker para distribuir o torrent.

Um rastreador é um serviço HTTP que deve ser contatado por um interlocutor para se juntar a um enxame (_swarm_). 

O arquivo torrent pode ser distribuído para outros usuários, sendo um website o meio mais comum para disponibiliza-lo, os trackers, citados anteriormente.

As Metainfos do .torrent estão escritos em bencoding (formato de codificação compacta de arquivos torrent para transmissão de metadados). Essas informações estão estruturados da seguinte forma: 
* 'announce': uma string que contém a URL do tracker; 
* 'created by':  pode conter o nome e a versão do programa usado para criar o arquivo metainfo;
* 'creation date': data de criação (opcional);
* 'info': contém um apontamento para um dicionário que contenha informações sobre os arquivos a serem baixados;

No dicionário são especificadas as entradas (informações sobre o arquivo):
* 'length': um valor inteiro que indica o comprimento do arquivo em bytes;
* 'md5sum': string de 32 caracteres correspondentes à soma MD5 do arquivo (opcional);
* 'name': string contendo o nome do arquivo;
* 'piece length': número inteiro que indica o número de bytes em cada peça;
* 'pieces': é um valor de cadeia contendo a concatenação do valor de hash SHA1 de 20 bytes para todas as peças no torrent. Por exemplo, os primeiros 20 bytes da string representam o valor SHA1 usado para verificar o índice de peça 0.


Portanto, o BitTorrent "quebra" os arquivos em pedaços de, geralmente, 256Kb. Os utilizadores da rede, partiham esses pedaços em ordem aleatória, que ao fim do download, os mesmos são juntados para forma o arquivo final. O sistema de partilha otimiza o desempenho geral de rede, uma vez que não existem filas de espera e todos partilham pedaços entre si, não sobrecarregando um servidor central, como acontece com sites e portais de downloads, por exemplo. Assim, quanto mais utilizadores entram para descarregar um determinado arquivo, mais largura de banda se torna disponível. []


## Experimentação:
Para este trabalho, a experimentação será baseada no Peerflix. 
O Peerflix é um aplicativo de streaming de vídeo, cliente BitTorrent para Node.js. Ele pode ser usado para transmissão de torrents de vídeo, via linha de comando e reproduzir em um player de vídeo, como o VLC.

## Instalação:
Primeiramente, precisamos instalar o Node.js e preparar o ambiente.
Usaremos o terminal do Linux para isto.

##### Instalar o Node.js
Para instalar o Node.js, pode ser feito baixando os arquivos no [site do Node.js](https://nodejs.org/en/) e instalando ou através do comando a seguir  (preferencialmente usando sudo):

`apt-get install nodejs`

Também instale o gerenciador de pacotes do Node.js. Você pode fazer isto digitando o comando a seguir:
`npm install`

##### Instalar o Peerflix
O código do Peerflix está disponível no [GitHub do projeto](https://github.com/mafintosh/peerflix) e também no Npm. Neste trabalho, nossas instalações serão pelo Npm, mas também disponibilizaremos o link para download por outros meios.

Para instalar o Peerflix, execute o seguinte comando:

`npm install -g peerflix`

## Como usar o Peerflix: 
Usaremos o VLC para visualizar o vídeo, os passos a seguir funcionam tanto para Windows como para Linux, desde que tenham o VLC instaldo.

O Peerflix pode ser usado com um link ou um arquivo torrent. 
Para transmitir um vídeo com seu link magnético, pode usar o seguinte comando:

`peerflix http://download.stefan.ubbink.org/ToS/tears_of_steel_1080p.webm.torrent --vlc﻿`

Executado o comando, será exibido as informações do streaming, conforme pode observar na imagem a seguir:

![Stream_Dados](imagens/info-stream.PNG)

Com o a URL informada, pode-se abrir o seu software preferido de reprodução de vídeos, que aceite transmissão, e digitar a URL fornecida. Assim, poderá ver os dados sobre o uso, inclusive as taxas de donwload e upload.

### Exemplo prático:
Para iniciar a transmissão e donwload do vídeo disponível, digite o seguinte comando:

`peerflix http://download.stefan.ubbink.org/ToS/tears_of_steel_1080p.webm.torrent --vlc﻿`

Assim, será exibido a tela com as informações do Streaming e em sequência abrirá o VLC, onde será execultado um vídeo. Observe na imagem a seguir:

![Stream_Dados](imagens/tela-de-reproducao.PNG)


#### Detalhes do Peerflix:
Para saber mais informações sobre o Peerflix, você pode digitar o seguinte comando: `peerflix --help` | Será exido uma tela com todas as informações sobre o mesmo.

![Opcoes](imagens/options-peerflix.PNG)


Os passos anteriores podem ser observados no vídeo a seguir:
[![Video1](https://img.youtube.com/vi/BxHaEXQnlNE/0.jpg)](https://www.youtube.com/watch?v=BxHaEXQnlNE)




#### Enriquecendo o Peerflix com Interface Gráfica (Navegador):
##### Peerflix Server

É possível ir além do terminal e usar o Peerflix com interface gráfica, através do navegador. Para isto, há a implementação do Peerflix Server. A mesma se encontra disponível no [GitHub](https://github.com/asapach/peerflix-server) e no repositório [Npm](https://www.npmjs.com/package/peerflix). 
 
 O Peerflix Server é baseado em [torrent-stream](https://github.com/mafintosh/torrent-stream), inspirado em peerflix.
 
 Observe a interface a seguir: 
 
 ![interface_peerflix_server](imagens/tela1-peerflix-server.gif)
*imagem retirada de [[8](https://github.com/asapach/peerflix-server)]

#### Vamos instalar?

Primeiramente, vamos aos pré-requisitos. 
O Peerflix Server usa o [Bower](https://bower.io/) e [Grunt](https://gruntjs.com/). O primeiro é um gerenciador de pacotes para a web, equanto o segundo é um automatizador de tarefas.

Instale o Bower usando o comando: `npm install -g bower `
* O Bower pode gerenciar componentes que contêm HTML, CSS, JavaScript, fontes ou até mesmo arquivos de imagem. A Bower não concatena ou diminui o código ou faz qualquer outra coisa - apenas instala as versões certas dos pacotes que você precisa e suas dependências [[4]](https://bower.io/).


**Instale o Grunt:**  `npm install -g grunt-cli`
* uma aplicação de linha de comando (roda em Node.js)que tem como objetivo automatizar tarefas, principalmente tarefas em aplicações JavaScript. (Ex.: realizar tarefas repetitivas como minificação, compilação, teste de unidade, linting e etc.)

* Na sequência, quando instalarmos o Peerflix Server, observe que o mesmo já criará os arquivos necessários do Grunt na pasta do Peerflix Server, apontando para o projeto. 

Os dois arquivos essenciais na pasta do Projeto para rodar o Grunt e as tarefas são: 

* **Gruntfile.js:** Esse é um arquivo JavaScript que são definidas e configuradas as tarefas a serem executadas pelo Grunt[[9]](https://tableless.com.br/grunt-voce-deveria-estar-usando/).

* **package.json:** Esse arquivo é usado pelo npm para armazenar as informações da aplicação, dados como nome, autor, repositório e dependências, e é por isso que o grunt precisa dele, para instalar as dependências e os plugins do grunt que seu projeto irá utilizar. Ao rodar o comando npm install, ele procura as dependências descritas nessa arquivo, e instala na pasta do projeto as mesmas com suas respectivas versões [[9]](https://tableless.com.br/grunt-voce-deveria-estar-usando/).
                                  

Conforme código a seguir:
```javascript
 {
      "_args": [
        [
          {
            "raw": "peerflix-server",
            "scope": null,
            "escapedName": "peerflix-server",
            "name": "peerflix-server",
            "rawSpec": "",
            "spec": "latest",
            "type": "tag"
          },
          "C:\\Users\\Paulo Henrique\\Google Drive\\TRABALHOS UFG\\Aplicações Distribuidas\\ad-si-projeto-4"
        ]
      ],...
  ```
      
                                   


Sugestão de uso: (Getting Started](https://gruntjs.com/getting-started)

 Supondo que você já instalou o Peerflix, conforme descrito anteriormente, vamos executar o comando:
 
`npm install -g peerflix-server`
 
#### Rodando o Peerflix
Agora para usar a interface com o Peerflix Server, basta iniciar o server, é muito simples. Execute o comando: 

`peerflix-server`

#### Como acessar?
Quando se inicia o server, será exibido no terminal o endereço para acesso através do navegador. Abra um navegador, preferencialmente Google Chrome ou Firefox, e digite o endereco informado.

`http://localhost:9000/`

Deverá abrir uma tela semelhante a da imagem a seguir:
 ![interface_peerflix_server](imagens/tela1-peerflix-server.gif)


#### Configurações:
É possível fazer algumas configurações no Peerflix Server, por exemplo, mudar a quantidade de conexões (limite) e a porta.

Crie um arquivo **config.json** em _~/.config/peerflix-server_ e faça as alterações necessárias. Por exemplo:

```javascript
{
  "connections": 50,
  "tmp": "/mnt/torrents"
}
```
_Obs: As opções são passadas para todas as instâncias torrent-stream._ 

Você também pode alterar a porta padrão configurando a variável de ambiente PORT :

```javascript
PORT=1234 peerflix-server
 
# or on windows 
SET PORT=1234
peerflix-server
``` 
A aplicação armazena seu estado atual (lista de torrents) em ~/.config/peerflix-server/torrents.json []





## Referências:
[1] [A Bíblia do Torrent](https://www.tecmundo.com.br/torrent/3175-a-biblia-do-torrent.htm)


[2] [Anatomia do BitTorrent a Ciência da Computação no Transmission](http://bcc.ime.usp.br/tccs/2013/rec/paulo/arquivos/monografia.pdf)


[3] [BitTorrent Protocol -- BTP/1.0](http://jonas.nitro.dk/bittorrent/bittorrent-rfc.html)

[4] [Bower](https://bower.io/)

[5] [Especificação do protocolo BitTorrent](https://www.klebermota.eti.br/2012/07/16/especificacao-do-protocolo-bittorrent/)

[6] [Grunt](https://gruntjs.com/)

[7] [Peerflix](https://github.com/mafintosh/peerflix)

[8] [Peerflix Server](https://github.com/mafintosh/peerflix)

[9][Grunt: você deveria estar usando!](https://tableless.com.br/grunt-voce-deveria-estar-usando/)
                                  
[10] [The BitTorrent Protocol Specification](http://www.bittorrent.org/beps/bep_0003.html)



